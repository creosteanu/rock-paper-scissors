"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
const _ = require("lodash");
const gatewayConfig = require("../gatewayConfig.json");
const baseLambdaUri = gatewayConfig.lambdaURI;
function createDeployment(gatewayService, deploy, projectApi, sha) {
    return __awaiter(this, void 0, void 0, function* () {
        return gatewayService.createDeployment({
            restApiId: projectApi.id,
            stageName: deploy.name,
            variables: deploy.variables,
            description: sha.substr(sha.length - 8)
        }).promise();
    });
}
function putMethod(gatewayService, { projectApi, method, resource }) {
    return __awaiter(this, void 0, void 0, function* () {
        yield gatewayService.putMethod({
            authorizationType: 'NONE',
            httpMethod: method.httpMethod,
            resourceId: resource.id,
            restApiId: projectApi.id
        }).promise();
    });
}
function updateVariables(deploy) {
    return _.map(deploy.variables, (value, key) => ({ op: 'replace', path: '/variables/' + key, value }));
}
function updateStage(gatewayService, deploy, projectApi, deployment) {
    return __awaiter(this, void 0, void 0, function* () {
        const updateStageVariables = updateVariables(deploy);
        updateStageVariables.push({ op: 'replace', path: '/deploymentId', value: deployment.id });
        gatewayService.updateStage({ restApiId: projectApi.id, stageName: deploy.name, patchOperations: updateStageVariables }).promise();
    });
}
function createResource(gatewayService, { projectApi, resourceName, parentId }) {
    return gatewayService.createResource({ pathPart: resourceName, restApiId: projectApi.id, parentId }).promise();
}
function getResource(gatewayService, projectApi, resourceName) {
    return __awaiter(this, void 0, void 0, function* () {
        const resources = yield gatewayService.getResources({ restApiId: projectApi.id }).promise();
        return resources.items.find((resource) => resource.pathPart === resourceName);
    });
}
function methodExists(gatewayService, { projectApi, method, resource }) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield gatewayService.getMethod({ restApiId: projectApi.id, resourceId: resource.id, httpMethod: method.httpMethod }).promise();
            return true;
        }
        catch (e) {
            return false;
        }
    });
}
function pushMethod(gatewayService, { method, projectApi, branchResource, branch, project }) {
    return __awaiter(this, void 0, void 0, function* () {
        let resource = branchResource;
        if (method.resource) {
            resource = yield getResource(gatewayService, projectApi, method.resource);
            resource = resource || (yield createResource(gatewayService, { projectApi, resourceName: method.resource, parentId: branchResource.id }));
        }
        if (!(yield methodExists(gatewayService, { projectApi, resource, method }))) {
            yield putMethod(gatewayService, { projectApi, method, resource });
        }
        yield gatewayService.putIntegration({
            httpMethod: method.httpMethod,
            resourceId: resource.id,
            restApiId: projectApi.id,
            type: 'AWS_PROXY',
            integrationHttpMethod: 'POST',
            credentials: gatewayConfig.Role,
            uri: `${baseLambdaUri}:${project}_${method.name}:${branch}/invocations`
        }).promise();
    });
}
function updateApi(gatewayService, { projectApi, branch, project, sha }) {
    return __awaiter(this, void 0, void 0, function* () {
        let branchResource = yield getResource(gatewayService, projectApi, branch);
        if (!branchResource) {
            const rootResource = yield getResource(gatewayService, projectApi, undefined);
            branchResource = yield createResource(gatewayService, { projectApi, resourceName: branch, parentId: rootResource.id });
        }
        for (const method of gatewayConfig.methods) {
            yield pushMethod(gatewayService, { method, projectApi, branchResource, branch, project });
        }
        const deployment = yield createDeployment(gatewayService, gatewayConfig.stages[0], projectApi, sha);
        for (const deploy of gatewayConfig.stages) {
            yield updateStage(gatewayService, deploy, projectApi, deployment);
        }
    });
}
function createApi(gatewayService, project) {
    return gatewayService.createRestApi({ name: project }).promise();
}
function getApi(gatewayService, project) {
    return __awaiter(this, void 0, void 0, function* () {
        const restApis = yield gatewayService.getRestApis().promise();
        return restApis.items.find((restApi) => restApi.name === project);
    });
}
function gatewayCI() {
    return __awaiter(this, void 0, void 0, function* () {
        const project = process.argv[2];
        const branch = process.argv[3];
        const sha = process.argv[4];
        const gatewayService = new AWS.APIGateway({ region: 'eu-central-1' });
        let projectApi = yield getApi(gatewayService, project);
        if (!projectApi) {
            projectApi = yield createApi(gatewayService, project);
        }
        yield updateApi(gatewayService, { projectApi, branch, project, sha });
        console.log('Gateway CI done');
    });
}
gatewayCI();
