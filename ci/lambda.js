"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
const lambdaConfig = require("../lambdaConfig.json");
function createAlias(lambdaService, lambdaFunction, branch) {
    return lambdaService.createAlias({ FunctionName: lambdaFunction.FunctionName, FunctionVersion: lambdaFunction.Version, Name: branch }).promise();
}
function updateAlias(lambdaService, lambdaFunction, branch) {
    return lambdaService.updateAlias({ FunctionName: lambdaFunction.FunctionName, FunctionVersion: lambdaFunction.Version, Name: branch }).promise();
}
function aliasExists(lambdaService, lambdaFunction, branch) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield lambdaService.getAlias({ FunctionName: lambdaFunction.FunctionName, Name: branch }).promise();
            return true;
        }
        catch (err) {
            return false;
        }
    });
}
function pushLambdaAlias(lambdaService, lambdaFunction, branch) {
    return __awaiter(this, void 0, void 0, function* () {
        if (yield aliasExists(lambdaService, lambdaFunction, branch)) {
            return updateAlias(lambdaService, lambdaFunction, branch);
        }
        else {
            return createAlias(lambdaService, lambdaFunction, branch);
        }
    });
}
function updateFunction(lambda, { lambdaFunctionName, S3Key }) {
    return lambda.updateFunctionCode({ FunctionName: lambdaFunctionName, S3Bucket: 'lambda.ci', S3Key }).promise();
}
function createFunction(lambda, { lambdaFunctionName, functionName, S3Key }) {
    return lambda.createFunction({
        FunctionName: lambdaFunctionName,
        Handler: `dist/main.${functionName}`,
        Runtime: 'nodejs6.10',
        Code: { S3Bucket: 'lambda.ci', S3Key },
        Role: lambdaConfig.Role,
        VpcConfig: lambdaConfig.VpcConfig
    }).promise();
}
function functionExists(lambda, FunctionName) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield lambda.getFunction({ FunctionName }).promise();
            return true;
        }
        catch (err) {
            return false;
        }
    });
}
function pushLambdaFunction(lambda, { lambdaFunctionName, S3Key, functionName }) {
    return __awaiter(this, void 0, void 0, function* () {
        if (yield functionExists(lambda, lambdaFunctionName)) {
            return updateFunction(lambda, { lambdaFunctionName, S3Key });
        }
        else {
            return createFunction(lambda, { lambdaFunctionName, functionName, S3Key });
        }
    });
}
function lambdaCI() {
    return __awaiter(this, void 0, void 0, function* () {
        const project = process.argv[2];
        const branch = process.argv[3];
        const S3Key = `${project}/${branch}.zip`;
        const lambdaService = new AWS.Lambda({ region: 'eu-central-1' });
        for (const functionName of lambdaConfig.functions) {
            const lambdaFunctionName = `${project}_${functionName}`;
            const lambdaFunction = yield pushLambdaFunction(lambdaService, { lambdaFunctionName, S3Key, functionName });
            yield pushLambdaAlias(lambdaService, lambdaFunction, branch);
        }
        console.log('Lambda CI done');
    });
}
lambdaCI();
