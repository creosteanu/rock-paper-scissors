import {getLastData} from './utils';
import * as _ from 'lodash';

function filterData(game) {
    return _.map(game, (data, player) => ({player, kills: data.kills, rounds: data.rounds}));
}

export async function getLeaderboard({stageVariables}, callback) {
    let {accessKeyId, secretAccessKey} = stageVariables;
    let awsCredentials = {accessKeyId, secretAccessKey};

    let game = await getLastData(awsCredentials, 'game');

    game = game || {};
    let leaderboard = filterData(game);

    callback(null, {statusCode: 200, body: JSON.stringify({leaderboard})});
}
