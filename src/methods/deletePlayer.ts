import {getLastData, dbPutItem} from './utils';

export async function deletePlayer({stageVariables, pathParameters}, callback) {
    let {accessKeyId, secretAccessKey} = stageVariables;
    let awsCredentials = {accessKeyId, secretAccessKey};

    let game = await getLastData(awsCredentials, 'game');

    let player = pathParameters && pathParameters.player;

    if (game && game[player]) {
        delete game[player];

        let timestamp = new Date().getTime() + '';
        let Item = {
            type: {S: 'game'},
            timestamp: {N: timestamp},
            value: {S: JSON.stringify(game)}
        };

        await dbPutItem(awsCredentials, {Item, TableName: 'Rocks'});
    }

    callback(null, {statusCode: 200, body: JSON.stringify({})});
}
