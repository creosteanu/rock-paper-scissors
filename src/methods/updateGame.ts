import {dbPutItem, getLastData} from './utils';
import * as _ from 'lodash';

function nextTick(game) {
    let mappedGame = _.map(game, (data, player) => ({...data, player}));
    let actions = _.groupBy(mappedGame, 'action');

    _.forEach(actions.rock, (data) => {
        game[data.player].kills += _.size(actions.scissors);

        if (_.isEmpty(actions.paper)) {
            game[data.player].rounds++;
        }
    });

    _.forEach(actions.scissors, (data) => {
        game[data.player].kills += _.size(actions.paper);

        if (_.isEmpty(actions.rock)) {
            game[data.player].rounds++;
        }
    });

    _.forEach(actions.paper, (data) => {
        game[data.player].kills += _.size(actions.rock);

        if (_.isEmpty(actions.scissors)) {
            game[data.player].rounds++;
        }
    });

    _.forEach(game, value => {
        delete value.action;
    });

    return game;
}

export async function updateGame({awsCredentials}) {
    let game = await getLastData(awsCredentials, 'game');

    if (!game) {
        return;
    }

    game = nextTick(game);

    let timestamp = new Date().getTime() + '';
    let Item = {
        type: {S: 'game'},
        timestamp: {N: timestamp},
        value: {S: JSON.stringify(game)}
    };

    await dbPutItem(awsCredentials, {Item, TableName: 'Rocks'});
}
