import {getLastData, dbPutItem} from './utils';

export async function playerAction({stageVariables, pathParameters, queryStringParameters}, callback) {
    let {accessKeyId, secretAccessKey} = stageVariables;
    let awsCredentials = {accessKeyId, secretAccessKey};

    let game = await getLastData(awsCredentials, 'game');
    game = game || {};

    let player =  pathParameters && pathParameters.player;
    if(player) {
        game[player] = game[player] || {rounds: 0, kills: 0};
        game[player].action = queryStringParameters && queryStringParameters.action && queryStringParameters.action.toLowerCase();

        let timestamp = new Date().getTime() + '';
        let Item = {
            type: {S: 'game'},
            timestamp: {N: timestamp},
            value: {S: JSON.stringify(game)}
        };

        await dbPutItem(awsCredentials, {Item, TableName: 'Rocks'});
    }

    callback(null, {statusCode: 200, body: JSON.stringify({})});
}
